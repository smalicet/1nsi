Groupe : Sana, Lalie

Compétence<br/>évaluée|note obtenue|barème|outils d'évaluation
--:|:--:|:--:|:--
Investissement dans le projet|          5        |5 |Journal de bord<br/>Attitude en classe<br/>Quantité de travail fourni
Contenu du site web |         2      |2| Le contenu est personnalisé<br/>Le contenu est cohérent et intéressant
Nombre de pages achevées |         3    |3|Le site comporte au minimum 4 pages par élève<br/>Les pages sont riches en contenus (liens, images, etc...)
Insertion de liens |           1      |1|Présence de liens internes au site<br/>Présence de liens vers d'autres sites vérifiés  
Présence des sources |          0          |1| Les sources et licences des images ou autres documents utilisés sont présentes
Insertion de code Javascript |      2       |3|Certaines pages du site possèdent du code javascript<br/>Qualité du javascript<br/>Variété dans   l'utilisation du javascript
Le fichier style |       1          |2|Le style est cohérent sur toutes les pages<br/>Le style est complexe avec de nombreux paramètres
Rendu final du projet |          3            |3|Appréciation personnelle sur le rendu final 
**Total**|                        17          |**20**|


Groupe : Maël, Thibaut, Alexandre

Compétence évaluée|note obtenue|barème|outils d'évaluation
--:|:--:|:--:|:--
Investissement dans le projet|           5       |5 |Journal de bord<br/>Attitude en classe<br/>Quantité de travail fourni
Contenu du site web |           2    |2| Le contenu est personnalisé<br/>Le contenu est cohérent et intéressant
Nombre de pages achevées |          3   |3|Le site comporte au minimum 4 pages par élève<br/>Les pages sont riches en contenus (liens, images, etc...)
Insertion de liens |              1   |1|Présence de liens internes au site<br/>Présence de liens vers d'autres sites vérifiés  
Présence des sources |              0     |1| Les sources et licences des images ou autres documents utilisés sont présentes
Insertion de code Javascript |       2      |3|Certaines pages du site possèdent du code javascript<br/>Qualité du javascript<br/>Variété dans   l'utilisation du javascript
Le fichier style |            2     |2|Le style est cohérent sur toutes les pages<br/>Le style est complexe avec de nombreux paramètres
Rendu final du projet |          3            |3|Appréciation personnelle sur le rendu final 
**Total**|                            18      |**20**|

Groupe : Tristan, Niels, Adrian

Compétence évaluée|note obtenue|barème|outils d'évaluation
--:|:--:|:--:|:--
Investissement dans le projet|            5      |5 |Journal de bord<br/>Attitude en classe<br/>Quantité de travail fourni
Contenu du site web |           2    |2| Le contenu est personnalisé<br/>Le contenu est cohérent et intéressant
Nombre de pages achevées |        3     |3|Le site comporte au minimum 4 pages par élève<br/>Les pages sont riches en contenus (liens, images, etc...)
Insertion de liens |              1   |1|Présence de liens internes au site<br/>Présence de liens vers d'autres sites vérifiés  
Présence des sources |             0      |1| Les sources et licences des images ou autres documents utilisés sont présentes
Insertion de code Javascript |      2       |3|Certaines pages du site possèdent du code javascript<br/>Qualité du javascript<br/>Variété dans   l'utilisation du javascript
Le fichier style |            2     |2|Le style est cohérent sur toutes les pages<br/>Le style est complexe avec de nombreux paramètres
Rendu final du projet |        3              |3|Appréciation personnelle sur le rendu final 
**Total**|                      18          |**20**|

Groupe : Louan, Guillem

Compétence évaluée|note obtenue|barème|outils d'évaluation
--:|:--:|:--:|:--
Investissement dans le projet|              5    |5 |Journal de bord<br/>Attitude en classe<br/>Quantité de travail fourni
Contenu du site web |          2     |2| Le contenu est personnalisé<br/>Le contenu est cohérent et intéressant
Nombre de pages achevées |      3       |3|Le site comporte au minimum 4 pages par élève<br/>Les pages sont riches en contenus (liens, images, etc...)
Insertion de liens |             1    |1|Présence de liens internes au site<br/>Présence de liens vers d'autres sites vérifiés  
Présence des sources |            0        |1| Les sources et licences des images ou autres documents utilisés sont présentes
Insertion de code Javascript |     3        |3|Certaines pages du site possèdent du code javascript<br/>Qualité du javascript<br/>Variété dans   l'utilisation du javascript
Le fichier style |           2      |2|Le style est cohérent sur toutes les pages<br/>Le style est complexe avec de nombreux paramètres
Rendu final du projet |       3               |3|Appréciation personnelle sur le rendu final 
**Total**|                     19            |**20**|

Groupe : Pierre, Cyran, Florian (non rendu, évalué d'après ce que j'ai vu en classe)

Compétence évaluée|note obtenue|barème|outils d'évaluation
--:|:--:|:--:|:--
Investissement dans le projet|            2 (+1)      |5 |Journal de bord<br/>Attitude en classe<br/>Quantité de travail fourni
Contenu du site web |        1       |2| Le contenu est personnalisé<br/>Le contenu est cohérent et intéressant
Nombre de pages achevées |      1 (+2)      |3|Le site comporte au minimum 4 pages par élève<br/>Les pages sont riches en contenus (liens, images, etc...)
Insertion de liens |             1    |1|Présence de liens internes au site<br/>Présence de liens vers d'autres sites vérifiés  
Présence des sources |            0       |1| Les sources et licences des images ou autres documents utilisés sont présentes
Insertion de code Javascript |     0 (+3)        |3|Certaines pages du site possèdent du code javascript<br/>Qualité du javascript<br/>Variété dans   l'utilisation du javascript
Le fichier style |          2     |2|Le style est cohérent sur toutes les pages<br/>Le style est complexe avec de nombreux paramètres
Rendu final du projet |      0         |3|Appréciation personnelle sur le rendu final 
**Total**|                 7 (+6)                |**20**|
