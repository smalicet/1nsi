class Pile:
    
    def __init__(self):
        """Construit une pile vide"""
        self.contenu = []
        
    def pile_vide(self):
        """Teste si une file est vide"""
        return len(self.contenu) == 0

    def depiler(self):
        """
        Extrait l'élément au sommet de la pile
        Coût constant, complexité en O(1)
        """
        assert not self.pile_vide(), "Pile Vide"
        return self.contenu.pop()
    
    def empiler(self, elt):
        """
        Insère elt au sommet de la pile
        Coût constant, complexité en O(1)
        """
        self.contenu.append(elt)

        
    # interface étendue
    def __str__(self):
        tmp = Pile()    
        while not self.pile_vide():
            tmp.empiler(self.depiler())
        sortie = 'Fin'
        while not tmp.pile_vide():
            sommet = tmp.depiler()
            sortie = f'{sommet} -> {sortie}'
            self.empiler(sommet)
        sortie = f'Début -> {sortie}'
        return sortie
    

def test_pile():
    """
    Tests unitaires de la classe Pile
    """
    p = Pile()
    for k in range(1, 6):
        p.empiler(k)
    print(p)
    for k in range(5, 0, -1):
        assert p.depiler() == k
    assert p.pile_vide()
    print("Tests réussis")
