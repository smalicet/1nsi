
# génération du graphe de cours d'informatique
liste_modules =  ['algorithmique', 'informatique commune',  'analyse',  'algèbre linéaire',  'informatique théorique',  'intelligence artificielle',  'apprentissage machine',  'calcul scientifique',  'programmation avancée',  'bases de données',  'bioinformatique',  'réseaux de neurones',  'robotique']
liste_arcs = [('algorithmique', 'bases de données'), ('algorithmique', 'calcul scientifique'), ('algorithmique', 'informatique théorique'), ('informatique commune', 'algorithmique'), ('informatique commune', 'programmation avancée'), ('analyse', 'algèbre linéaire'), ('algèbre linéaire', 'informatique théorique'), ('informatique théorique', 'bioinformatique'), ('informatique théorique', 'intelligence artificielle'), ('intelligence artificielle', 'apprentissage machine'), ('intelligence artificielle', 'robotique'), ('intelligence artificielle', 'réseaux de neurones'), ('apprentissage machine', 'réseaux de neurones'), ('calcul scientifique', 'bioinformatique'), ('programmation avancée', 'calcul scientifique')]

graphe_info = Graphe_oriente(liste_modules)
for i, j in liste_arcs:
    graphe_info.ajoute_arc(i, j)


def ordre_topologique(graphe):
    """
    Renvoie un ordre topologique d'un graphe orienté supposé sans cycle
    sous la forme d'une liste Python avec les sommets ordonnés
    de gauche à droite
    """
    fileA = File()
    for s in graphe.sommets():
        if graphe.degre_entrant(s) == 0:
            fileA.enfiler(s)
    fileB = File()
    #à compléter  
    ordre = []
    while not fileB.file_vide():
        ordre.append(fileB.defiler())
    return ordre


def test_ordre_topologique():
    """
    Test unitaire pour la fonction ordre_topologique    
    """
    assert ordre_topologique(graphe_info) == ['informatique commune',  'analyse',  'algorithmique',
  'programmation avancée',   'algèbre linéaire',   'bases de données',   'calcul scientifique',
  'informatique théorique',   'bioinformatique',   'intelligence artificielle',   'apprentissage machine',
  'robotique',   'réseaux de neurones'], "échec sur ordre_topologique(graphe_info)"
    print("Test unitaire réussi")
    
def dfs_topo(sommet, graphe, decouvert):
    """
    Parcours en profondeur récursif augmenté 
    Marque dans  le dictionnaire decouvert  que sommet est découvert au cours du parcours
    Affiche le sommet en fin d'appel
    """
    # à compléter

    
def ordre_topologique2(graphe):
    decouvert = {s: False for s in graphe_info.sommets()}
    for s in graphe.sommets():
        if not decouvert[s]:
            dfs_topo(s, graphe, decouvert)
