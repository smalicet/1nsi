import ctree




def pgcd(a, b):
    while b != 0:
        if a > b:
            a = a - b
        else:
            b = b - a
    return a

tree1 = ctree.get_ast(pgcd)

print(tree1.to_dot())