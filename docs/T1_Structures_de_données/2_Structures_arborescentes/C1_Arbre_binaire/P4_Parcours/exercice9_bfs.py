from collections import deque

class Noeud2:
    """
    Noeud pour arbre binaire

    On redéfinit une classe Noeud2 pour ne pas avoir de conflit
    avec la classe Noeud de l'exercice 8    
    """
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)
    

class File:
    
    def __init__(self):
        self.contenu = deque([])
        
    def file_vide(self):
        return len(self.contenu) == 0

    def defiler(self):
        assert not self.file_vide(), "File Vide"
        return self.contenu.popleft()
    
    def enfiler(self, elt):
        self.contenu.append(elt)
        
def parcours_largeur(arbre):
    # à compléter
    ...
   
        
# construction de l'arbre binaire
a = Noeud2(Noeud2(None, 'R', Noeud2(None, 'A', None)), 'B', Noeud2(Noeud2(None, 'D', None), 'E', Noeud2(Noeud2(None, 'H', None), 'T', None)))
# à vous de tester votre fonction parcours_largeur