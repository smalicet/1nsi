"""
# Une classe Noeud pour arbre binaire.

un noeud possède trois attributs :

gauche : lien vers fils gauche (ou None si vide)
element : lien vers élément stocké 
droit : lien vers fils droit (ou None si vide)

# Deux interfaces pour arbre binaire  basées sur la classe Noeud :

## Interface fonctionnelle (arbre binaire immuable)

arbre_vide()
est_vide(arbre)
gauche(arbre)
droit(arbre)
element_racine(arbre)
creer_arbre(fils_gauche, element, fils_droit)
afficher_arbre(arbre)
afficher_arbre_joli(arbre)
to_dot(arbre)

## Interface objet (arbre binaire mutable au niveau de la racine ou des enfants vides )

__init__(self)
est_vide(self)
gauche(self)
droit(self)
element_racine(self)
ajout_racine(self, element)
afficher_arbre(self)
to_dot(self)
__str__(self)
"""


from collections import deque

class Noeud:
    """Classe de Noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        """Construit un objet de la classe Noeud
        self.gauche : lien vers fils gauche g éventuellement vide (None)
        self.element : élément e stocké dans le noeud
        self.droit : lien vers fils droit d éventuellement vide (None)
        """
        self.gauche = g 
        self.element = e 
        self.droit = d 

#%% Interface fonctionnelle pour le type arbre binaire

def arbre_vide():
    """
    Fonction de l'interface fonctionnelle d'arbre binaire
    Renvoie un arbre vide représenté par None
    """
    return None

def est_vide(arbre):
    """
    Fonction de l'interface fonctionnelle d'arbre binaire
    Teste si un arbre est vide, renvoie un booléen
    """
    return arbre is None

def gauche(arbre):
    """
    Fonction de l'interface fonctionnelle d'arbre binaire
    Renvoie le sous-arbre fils gauche de arbre 
    Provoque une erreur si arbre est vide
    """
    assert not est_vide(arbre), "Arbre vide"
    return arbre.gauche

def droit(arbre):
    """
    Fonction de l'interface fonctionnelle d'arbre binaire
    Renvoie le sous-arbre fils droit de arbre 
    Provoque une erreur si arbre est vide
    """
    assert not est_vide(arbre), "Arbre vide"
    return arbre.droit

def element_racine(arbre):
    """
    Fonction de l'interface fonctionnelle d'arbre binaire
    Renvoie l'élément à la racine de arbre
    Provoque une erreur si arbre est vide
    """
    assert not est_vide(arbre), "Arbre vide"
    return arbre.element

def creer_arbre(g, e, d):
    """Construit et renvoie l'arbre binaire dont la racine est constituée
    par le noeud d'élément e, g sous-arbre gauche et d sous-arbre droit"""
    return Noeud(g, e, d)

def afficher_arbre(arbre):
    """
    Fonction de l'interface fonctionnelle d'arbre binaire
    Affichage syntaxiquement correct d'un arbre contruit avec la classe Noeud
    Analogue à la fonction builtin str
    """
    if arbre is None:
        return repr(None)
    return f"Noeud({afficher_arbre(arbre.gauche)}, {repr(arbre.element)}, {afficher_arbre(arbre.droit)})"

def afficher_arbre_joli(arbre):
    """Affichage joli d'un arbre binaire construit avec la classe Noeud
    Analogue à la fonction builtin str"""    
    
    def aux(arbre):
        """Fonction récursive auxiliaire"""
        if arbre is None:
            return ['']
        lignes_sag = aux(arbre.gauche)
        lignes_sad  = aux(arbre.droit)
        decalage_horizontal = 2 + len(str(arbre.element))
        rep = str(arbre.element) +  '_' * 2 + lignes_sag[0] + '\n'
        for ligne in lignes_sag[1:]:
            rep = rep + '|' +  ' ' * (decalage_horizontal - 1) + ligne + '\n'
        rep = rep + '|\n'
        rep = rep +  '|' + '_' *  (decalage_horizontal - 1) + lignes_sad[0] + '\n'        
        for ligne in lignes_sad[1:]:
            rep = rep + ' ' *  decalage_horizontal + ligne + '\n'
        rep = rep.rstrip()
        return rep.split('\n')
    
    
    rep = aux(arbre)
    return '\n'.join(rep)

def to_dot(arbre, path='binarytree.dot'):
        """
        Fonction de l'interface fonctionnelle d'arbre binaire
        Renvoie une représentation de l'arbre sous forme de chaine de caractères
        au format dot, à coller sans les délimiteurs ' et '
        dans http://dreampuf.github.io/GraphvizOnline/
        """
        lignes = ["digraph {edge [dir = none];"]             
        numero_noeud = 0
        file = deque([(arbre, numero_noeud)])  
        pere = {(arbre, numero_noeud): None}
        while len(file) > 0:
            courant, numero = file.popleft()
            if not est_vide(courant):
                lignes.append(str(numero) + f'[label="{courant.element}"]' + ";")
            else:
                lignes.append(str(numero) + '[shape=circle, style = invis]' + ";")
            if pere[(courant, numero)] is not None:
                lignes.append(str(pere[(courant, numero)])  + '->' +   str(numero)  + ";")
            if not est_vide(courant):
                fg = courant.gauche
                fd = courant.droit               
                numero_noeud += 1
                pere[(fg, numero_noeud)] = numero
                file.append((fg, numero_noeud ))
                numero_noeud += 1
                pere[(fd, numero_noeud)] = numero
                file.append((fd, numero_noeud))
        lignes.append("}")
        contenu = "".join(lignes)
        f = open(path, mode='w')
        f.write(contenu)
        f.close()
        return contenu

#%%  Implémentation objet du type arbre binaire avec deux classes Noeud déjà définie et AB , arbres mutables

class AB:
    """Classe d'arbre binaire mutable"""
        
    def __init__(self):
        """Constructeur, self.racine point vers None si arbre vide
        ou le noeud racine"""
        self.racine = None

    def est_vide(self):
        """Teste si l'arbre est vide, renvoie un booléen"""
        return self.racine is None

    def droit(self):
        """Renvoie le sous-arbre (de type Arbre) fils droit de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.droit
    
    def gauche(self):
        """Renvoie le sous-arbre (de type Arbre) fils gauche de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.gauche
    
    def ajout_racine(self, element):
        """Ajoute un noeud stockant element à la racine de l'arbre
        Provoque une erreur si arbre est non vide"""
        assert self.est_vide()
        self.racine = Noeud(AB(), element, AB())
    
    def element_racine(self):
        """Renvoie l'élément stocké dans le noeud racine de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.element
    
    def to_dot(self, path='binarytree.dot'):		
        """Renvoie une représentation de l'arbre sous forme de chaine de caractères
        au format dot, à coller sans les délimiteurs ' et '
        dans http://dreampuf.github.io/GraphvizOnline/"""
        lignes = ["digraph {edge [dir = none];"]             
        numero_noeud = 0
        file = deque([(self, numero_noeud)])  
        pere = {(self, numero_noeud): None}
        while len(file) > 0:
            courant, numero = file.popleft()
            if not courant.est_vide():
                lignes.append(str(numero) + f'[label="{courant.racine.element}"]' + ";")
            else:
                lignes.append(str(numero) + '[shape=circle, style = invis]' + ";")
            if pere[(courant, numero)] is not None:
                lignes.append(str(pere[(courant, numero)])  + '->' +   str(numero)  + ";")
            if not courant.est_vide():
                fg = courant.gauche()
                fd = courant.droit()              
                numero_noeud += 1
                pere[(fg, numero_noeud)] = numero
                file.append((fg, numero_noeud ))
                numero_noeud += 1
                pere[(fd, numero_noeud)] = numero
                file.append((fd, numero_noeud))
        lignes.append("}")
        contenu = "".join(lignes)
        f = open(path, mode='w')
        f.write(contenu)
        f.close()
        return contenu
        
    def __str__(self):
        """Affichage joli d'un arbre binaire construit avec la classe Noeud
		Analogue à la fonction builtin str"""    

        def aux(arbre):
            """Fonction récursive auxiliaire"""
            if arbre.est_vide():
                return ['']
            lignes_sag = aux(arbre.gauche())
            lignes_sad  = aux(arbre.droit())
            decalage_horizontal = 2 + len(str(arbre.element_racine()))
            rep = str(arbre.element_racine()) + '_' * 2 + lignes_sag[0] + '\n'
            for ligne in lignes_sag[1:]:
                rep = rep + '|' +  ' ' * (decalage_horizontal - 1) + ligne + '\n'
            rep = rep + '|\n'
            rep = rep +  '|' + '_' * (decalage_horizontal - 1) + lignes_sad[0] + '\n'        
            for ligne in lignes_sad[1:]:
                rep = rep + ' ' * decalage_horizontal + ligne + '\n'
            rep = rep.rstrip()
            return rep.split('\n')

        rep = aux(self)
        return '\n'.join(rep)
        
	
