"""
Interface fonctionnelle avec classe Noeud pour ABR immuable
"""
class Noeud:
    """Noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)
        
# Interface fonctionnelle minimale
    
def abr_vide():
    """Renvoie un arbre binaire de recherche vide représenté par None"""
    return None

def est_vide(abr):
    """Teste si un arbre binaire de recherche est vide, renvoie un booléen"""
    return abr is None

def gauche(abr):
    """Renvoie le sous-arbre fils gauche de  l'arbre binaire de recherche abr
    Provoque une erreur si arbre est vide"""
    assert not est_vide(abr), "Arbre vide"
    return abr.gauche

def droit(abr):
    """Renvoie le sous-arbre fils droit de  l'arbre binaire de recherche abr
    Provoque une erreur si arbre est vide"""
    assert not est_vide(abr), "Arbre vide"
    return abr.droit

def element_racine(abr):
    """Renvoie l'élément à la racine de l'arbre binaire de recherche abr
    Provoque une erreur si arbre est vide"""
    assert not est_vide(abr), "Arbre vide"
    return abr.element

# extension de l'interface
def ajoute(abr, elt):
    """Renvoie un nouvel arbre binaire de recherche construit par ajout de elt
    comme feuille dans l'arbre binaire de recherche abr
    """
    if abr is None:
        return Noeud(None, elt, None)
    elif elt <= abr.element:
        return Noeud(ajoute(abr.gauche, elt), abr.element, abr.droit)
    else:
        return Noeud(abr.gauche, abr.element, ajoute(abr.droit, elt))
    
def recherche(abr, elt):
    """Renvoie un booléen indiquant si elt est stocké dans un noeud 
    de l'arbre binaire de recherche abr
    """
    if abr is None:
        return False
    elif elt < abr.element:
        return recherche(abr.gauche, elt)
    elif elt > abr.element:
        return recherche(abr.droit, elt)
    else:
        return True    

def maximum(abr):
    """Renvoie le maximum de l'arbre  binaire de recherche abr"""
    assert not est_vide(abr), "arbre vide"
    if est_vide(abr.droit):
        return abr.element
    return maximum(abr.droit)

def minimum(abr):
    """Renvoie le minimum de l'arbre  binaire de recherche abr"""
    assert not est_vide(abr), "arbre vide"
    if est_vide(abr.gauche):
        return abr.element
    return minimum(abr.gauche)

    
def parcours_infixe(abr):
    """Renvoie une trace du parcours infixe de l'arbre binaire de recherche
    dans un tableau
    """
    rep = []
    if est_vide(abr):
        return rep
    rep.extend(parcours_infixe(abr.gauche))
    rep.append(abr.element)    
    rep.extend(parcours_infixe(abr.droit))
    return rep
    
def afficher_arbre(abr):
    """Affichage syntaxiquement correct d'un arbre binaire de recherche abr construit avec la classe Noeud
    Analogue à la fonction builtin repr"""
    if est_vide(abr):
        return repr(None)
    return f"Noeud({afficher_arbre(abr.gauche)}, {repr(abr.element)}, {afficher_arbre(abr.droit)})"

def afficher_arbre_joli(arbre):
    """Affichage joli d'un arbre binaire construit avec la classe Noeud
    Analogue à la fonction builtin str"""    
    
    def aux(arbre):
        """Fonction récursive auxiliaire"""
        if arbre is None:
            return ['']
        lignes_sag = aux(arbre.gauche)
        lignes_sad  = aux(arbre.droit)
        decalage_horizontal = 2 + len(str(arbre.element))
        rep = str(arbre.element) +  '_' * 2 + lignes_sag[0] + '\n'
        for ligne in lignes_sag[1:]:
            rep = rep + '|' +  ' ' * (decalage_horizontal - 1) + ligne + '\n'
        rep = rep + '|\n'
        rep = rep +  '|' + '_' *  (decalage_horizontal - 1) + lignes_sad[0] + '\n'        
        for ligne in lignes_sad[1:]:
            rep = rep + ' ' *  decalage_horizontal + ligne + '\n'
        rep = rep.rstrip()
        return rep.split('\n')
    
    rep = aux(arbre)
    return '\n'.join(rep)


def hauteur(abr):
    """Renvoie la hauteur de l'arbre  binaire de recherche abr"""
    if est_vide(abr):
        return 0
    return 1 + max(hauteur(droit(abr)), hauteur(gauche(abr)))
                   

def taille(abr):
    """Renvoie la hauteur de l'arbre  binaire de recherche abr"""
    if est_vide(abr):
        return 0
    return 1 + taille(droit(abr)) + taille(gauche(abr))

def propriete_abr(abr, minorant=-float('inf'), majorant=float('inf')):
    """Vérifie si un arbre binaire abr 
    satisfait la propriete d'arbre binaire de recherche"""
    if est_vide(abr):
        return True
    e = element_racine(abr)
    prop_gauche = propriete_abr(abr.gauche, minorant, e)
    prop_droit = propriete_abr(abr.droit, e, majorant)
    prop_racine = (minorant <= e and e <= majorant)
    return prop_gauche and prop_droit and prop_racine
                   
from collections import deque 

def to_dot(arbre, path='binarytree.dot'):
        """Renvoie une représentation de l'arbre sous forme de chaine de caractères
        au format dot, à coller sans les délimiteurs ' et '
        dans http://dreampuf.github.io/GraphvizOnline/"""
        lignes = ["digraph {edge [dir = none];"]             
        numero_noeud = 0
        file = deque([(arbre, numero_noeud)])  
        pere = {(arbre, numero_noeud): None}
        while len(file) > 0:
            courant, numero = file.popleft()
            if not est_vide(courant):
                lignes.append(str(numero) + f'[label="{courant.element}"]' + ";")
            else:
                lignes.append(str(numero) + '[shape=circle, style = invis]' + ";")
            if pere[(courant, numero)] is not None:
                lignes.append(str(pere[(courant, numero)])  + '->' +   str(numero)  + ";")
            if not est_vide(courant):
                fg = courant.gauche
                fd = courant.droit               
                numero_noeud += 1
                pere[(fg, numero_noeud)] = numero
                file.append((fg, numero_noeud ))
                numero_noeud += 1
                pere[(fd, numero_noeud)] = numero
                file.append((fd, numero_noeud))
        lignes.append("}")
        contenu = "".join(lignes)
        f = open(path, mode='w')
        f.write(contenu)
        f.close()
        return contenu
    
# tests unitaires
def test_abr_immuable():
    a = Noeud(Noeud(None, 4, Noeud(None, 6, None)), 8, Noeud(Noeud(None, 9, None), 10, Noeud(None, 12, None)))
    assert taille(a) == 6
    assert hauteur(a) == 3
    assert maximum(a) == 12
    assert minimum(a) == 4
    assert propriete_abr(a) == True
    assert parcours_infixe(a) == [4, 6, 8, 9, 10, 12]
    b = Noeud(Noeud(None, 4, Noeud(None, 3, None)), 8, Noeud(Noeud(None, 9, None), 10, Noeud(None, 12, None)))
    assert propriete_abr(b) == False
    print("Tests 1 réussis")
    
def test_abr_immuable2():
    a = Noeud(Noeud(None, 4, Noeud(None, 6, None)), 8, Noeud(Noeud(None, 9, None), 10, Noeud(None, 12, None)))
    assert recherche(a, 8) == True
    assert recherche(a, 6) == True
    assert recherche(a, 12) == True
    assert recherche(a, 13) == False
    assert recherche(a, 7) == False
    print("Tests 2 réussis")
    
    
if __name__ == "__main__":
    test_abr_immuable()
    test_abr_immuable2()
    
