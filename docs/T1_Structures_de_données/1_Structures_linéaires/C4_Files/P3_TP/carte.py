import random

class Carte: 
    
    # attributs de classe partagés par toutes les instances de la classe
    hauteurs_cartes = [str(k) for k in range(2, 11)] + ['V', 'D', 'R', 'A']
    couleurs_cartes = ['Trêfle', 'Carreau', 'Coeur', 'Pique']
    
    def __init__(self, h, c):
        """Construit une carte de hauteur h et de couleur c"""
        assert h in Carte.hauteurs_cartes, "hauteur de carte doit être dans '2,3,4,5,6,7,8,9,10,V,D,R,A'"
        assert c in Carte.couleurs_cartes, "couleur de carte  doit être dans ['Trêfle', 'Carreau', 'Coeur', 'Pique']"
        self.hauteur = h
        self.couleur = c
    
    def valeur_bataille(self):
        """Renvoie l'index de la hauteur de la carte dans Carte.hauteurs_cartes
        cet entier peut être comparé avec des hauteurs d'autres cartes"""
        return Carte.hauteurs_cartes.index(self.hauteur)
    
    def __repr__(self):
        """Représentation textuelle syntaxiquement correcte de la carte pour affichage en console ou avec repr.
        Permet de constuire la carte avec eval(repr(self))"""
        return f"Carte({self.hauteur}, {self.couleur})"
    
    def __str__(self):
        """Représentation textuelle jolie de la carte. Pour affichage avec print ou str"""
        return f"({self.hauteur}, {self.couleur})"
