class Cellule:
    """Cellule pour liste chaînée"""

    def __init__(self, elt, suivant):
        self.element = elt
        self.suivant = suivant
        
class File:
    """Implémentation du type abstrait File
    par une liste chaînée mutable"""

    def __init__(self):
        """Constructeur de file vide"""
        self.debut = None
        self.fin = None
        
    def file_vide(self):
        """Teste si une file est vide"""
        return (self.debut is None) and (self.fin is None)

    def defiler(self):
        """Retire et renvoie l'élément pointé par self.debut"""
        assert not self.file_vide(), "File Vide"
        # à compléter
        ...
       
      
    def enfiler(self, elt):
        """Insère elt à la suite de self.fin"""
        # à compléter
        ...
    
    # interface étendue 
    
    def queue(self):
        assert not self.file_vide()           
        file_queue = File()
        file_queue.debut = self.debut.suivant
        file_queue.fin = self.fin
        return file_queue
    
    def affichage_aux(self):
        if self.file_vide():
            return 'None'
        return f"{str(self.debut.element)} -> {self.queue().affichage_aux()}"
    
    def __str__(self):
        return "début : " + self.affichage_aux() + " : fin"
        


def test_file():
    f = File()
    for k in range(1, 6):
        f.enfiler(k)
    for k in range(1, 6):
        assert f.defiler() == k
    assert f.file_vide()
    print("Tests réussis")
    