class Cellule:
    """Cellule pour liste chaînée"""
    
    def __init__(self, elt, suivant):
        self.element = elt
        self.suivant = suivant
        

        
class Liste:    
    """Liste chaînée mutable"""
    
    # Interface minimale du type abstrait Liste
    
    def __init__(self):
        """Construit une liste vide"""
        self.tete_liste = None
        
    def liste_vide(self):
        """Teste si la liste est vide"""
        return self.tete_liste is None

    def inserer(self, elt):
        """Insère elt en tête de liste"""
        self.tete_liste = Cellule(elt, self.tete_liste)
    
    def tete(self):
        """Renvoie l'élement de la cellule en tête de liste"""
        assert not self.liste_vide()
        return self.tete_liste.element
        
    def queue(self):
        """Renvoie une nouvelle liste qui commence avec la  cellule suivant
        la cellule en tête de la liste courante
        """
        assert not self.liste_vide()           
        liste_queue = Liste()
        liste_queue.tete_liste = self.tete_liste.suivant
        return liste_queue
    
       
    # Extension de l'interface
    
    def __str__(self):
        if self.liste_vide():
            return '()'
        return f"({str(self.tete_liste.element)}, {str(self.queue())})"
    
    def longueur(self):
        """Renvoie la longueur de la liste"""
        # à compléter
        ...
    
    def modifier_tete(self, elt):        
        """Modifie l'éléme t de la cellule en tête de liste
        Méthode de liste mutable
        """
        assert not self.liste_vide()
        # à compléter
        ...
        
    def renverse_liste_iter(self):
        """Inverse la séquence des éléments dans la liste, version itérative
        Méthode de liste mutable
        """
        # à compléter
        lis_tmp = Liste()
        lis = self
        while not ...:
            ...
        self.tete_liste = lis_tmp.tete_liste
        
    def renverse_liste_rec(self, lis_tmp):
        """Inverse la séquence des éléments dans la liste, version récursive
        le paramètre lis_tmp est une liste vide lors du premier appel
        Méthode de liste mutable
        """
        # à compléter
        # si liste vide on ne fait rien
        if self.liste_vide():
            return
        # sinon on insère l'élément  en tête de liste dans  lis_tmp
        lis_tmp.inserer(...)
        # puis on appelle récursivement la méthode sur la queue de la liste
        ...
        # enfin on fait pointer l'attribut tete_liste de la liste vers lis_tmp.tete_liste
        self.tete_liste = lis_tmp.tete_liste
        
def test():
    l1 = Liste()
    for k in range(10):
        l1.inserer(k)
    assert str(l1) == '(9, (8, (7, (6, (5, (4, (3, (2, (1, (0, ()))))))))))'
    l1.modifier_tete(99)
    assert str(l1) == '(99, (8, (7, (6, (5, (4, (3, (2, (1, (0, ()))))))))))'
    l1.renverse_liste_rec(Liste())
    assert str(l1) == '(0, (1, (2, (3, (4, (5, (6, (7, (8, (99, ()))))))))))'
    l1.renverse_liste_iter()
    assert str(l1) == '(99, (8, (7, (6, (5, (4, (3, (2, (1, (0, ()))))))))))'
    print("Tests réussis")
        
        
