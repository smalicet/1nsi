class Cellule:
    
    def __init__(self, elt, suivant):
        self.element = elt
        self.suivant = suivant
        
def creer_liste():
    return None

def liste_vide(lis):
    return lis is None

def inserer(lis, elt):
    """Insère elt en  tête de lis et renvoie la  liste créée"""
    # à compléter
    ...


def tete(lis):
    """Renvoie l'élement en tête de la liste lis"""
    # à compléter
    ...

def queue(lis):
    """Renvoie une liste qui est la queue de lis"""
    # à compléter
    ...


def test():
    lis = None 
    assert liste_vide(lis)
    for k in range(0, 3):
        lis = inserer(lis, k)
    assert lis.element == 2
    lis = lis.suivant
    assert lis.element == 1
    lis = lis.suivant
    assert lis.element == 0
    print("Tests réussis")
    