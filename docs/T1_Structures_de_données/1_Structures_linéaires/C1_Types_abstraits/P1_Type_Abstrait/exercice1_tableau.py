def creer_tableau(taille):
    return [None for _ in range(taille)]


def lire_case(tableau, index):
    assert isinstance(index, int), "index doit être un entier"
    assert 0 <= index < len(tableau), "index en dehors de la plage licite"
    # à compléter

def modifier_case(tableau, index, valeur):
    assert isinstance(index, int), "index doit être un entier"
    assert 0 <= index < len(tableau), "index en dehors de la plage licite"
    # à compléter
    
def test_tableau():
    numeros = creer_tableau(10)
    modifier_case(numeros, 2, "0642454712")
    assert lire_case(numeros, 2) == "0642454712"
    modifier_case(numeros, 0, "0842454912")
    assert lire_case(numeros, 0) == "0842454912"
    print("Tests réussis")