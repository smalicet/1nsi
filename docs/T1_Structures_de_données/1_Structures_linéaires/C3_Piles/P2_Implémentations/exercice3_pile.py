class Pile:
    
    def __init__(self, taille_max):
        self.taille_max = taille_max
        self.taille  = 0
        self.tab = [None for _ in range(self.taille_max)]

    def pile_vide(self):
        # à compléter
        ...

    def depiler(self):
        assert not self.pile_vide(), "Pile Vide"
        # à compléter
        ...
        

    def empiler(self, elt):
        assert self.taille < self.taille_max, "Dépassement de capacité"
        # à compléter
        ...
        
    def __str__(self):
        tmp = Pile(self.taille_max)   
        while not self.pile_vide():
            tmp.empiler(self.depiler())
        sortie = 'None'
        while not tmp.pile_vide():
            sommet = tmp.depiler()
            sortie = f'({sommet},{sortie})'
            self.empiler(sommet)
        return sortie
    
def test_pile():
    stack = Pile(5)
    for k in [8, 4, 3]:
        stack.empiler(k)
    assert str(stack) == '(3,(4,(8,None)))'
    for k in [3, 4, 8]:
        sommet = stack.depiler()
        assert sommet == k
    assert stack.pile_vide() == True
    print("tests réussis")