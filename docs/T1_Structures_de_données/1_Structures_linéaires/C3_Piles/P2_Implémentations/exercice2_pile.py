# Interface du type abstrait Pile
def creer_pile():
    return []
    
def pile_vide(pile):
    return pile == []
    
def depiler(pile):
    sommet = pile.pop()
    return sommet
    
def empiler(pile, elt):
    pile.append(elt)
    
# interface étendue
def longueur(pile):
    autre = creer_pile()
    k = 0
    # à compléter
    ...
    

def valeur_sommet(pile):
    sommet = depiler(pile)
    empiler(pile, sommet)
    return sommet

def str_pile(pile):
    tmp = creer_pile()    
    while not pile_vide(pile):
        empiler(tmp, depiler(pile))
    sortie = 'None'
    while not pile_vide(tmp):
        sommet = depiler(tmp)
        sortie = f'({sommet},{sortie})'
        empiler(pile, sommet)
    return sortie

# tests
def test_pile():
    pile = creer_pile()
    for k in [8, 4, 3]:
        empiler(pile, k)
    assert str_pile(pile) == '(3,(4,(8,None)))'
    assert longueur(pile) == 3, "echec sur longueur(pile) == 3"
    for k in [3, 4, 8]:
        sommet = depiler(pile)
        assert sommet == k
    assert pile_vide(pile) == True
    assert longueur(pile) == 0, "echec sur longueur(pile) == 0"
    print("tests réussis")
    