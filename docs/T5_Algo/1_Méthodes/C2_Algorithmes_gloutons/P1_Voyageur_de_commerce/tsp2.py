class TSP2:
    """Classe pour problème du voyageur de commerce"""
    
    def __init__(self, v, d):
        # liste de noms de villes
        self.nom_ville = v 
        # nombre de villes
        self.nb_villes = len(self.nom_ville) 
        # dictionnaire :  nom de ville -> index dans self.nom_ville
        self.index_ville = {self.nom_ville[k]: k  for k in range(self.nb_villes)}
        # tableau 2d des distances entre villes (repérées par leur index)
        self.distance = d      
        # tableau de marques des villes déjà visités dans le circuit glouton
        self.visite = [False for _ in range(self.nb_villes)]
        
    def plus_proche(self, va):
        """Renvoie le couple (distance minimale, index ville la plus proche)
        pour une ville d'index va dans self.nom_ville
        """
        dmin = float('inf')
        for vb in range(self.nb_villes):
            if vb != va and (not self.visite[vb]) and  self.distance[va][vb] < dmin:
                # plusieurs lignes à compléter
                ...
        return (dmin, vmin)

    def meilleur_circuit_glouton(self, nom_depart):
        """
        Renvoie le couple (distance totale circuit, liste de noms
        des villes d'un circuit hamiltonien construit par heuristique gloutonne)
        """
        vd = self.index_ville[nom_depart]
        derniere_etape = vd
        circuit = [derniere_etape]      
        self.visite[derniere_etape] = True
        dist_totale = 0
        for _ in range(self.nb_villes - 1):
            dist_etape, prochaine_etape = self.plus_proche(derniere_etape)
            # plusieurs lignes à compléter
            ...
        circuit.append(circuit[0])
        dist_totale  = dist_totale + self.distance[circuit[-2]][circuit[-1]]        
        return (dist_totale, [self.nom_ville[v] for v in circuit])
    
    
def test_meilleur_circuit_glouton():
    nom_ville = ['Nancy', 'Metz', 'Paris', 'Reims', 'Troyes' ]
    distance = [[0, 55, 303, 188, 183], 
                [55, 0, 306, 176, 203], 
                [303, 306, 0, 142, 153],
                [188, 176, 142, 0, 123], 
                [183, 203, 153, 123, 0]]
    tsp2 = TSP2(nom_ville, distance)
    (dmin, vmin) = tsp2.meilleur_circuit_glouton("Nancy")
    vmin_nancy = vmin[vmin.index('Nancy'):-1] + vmin[:vmin.index('Nancy') + 1]
    attendu = ['Nancy', 'Metz', 'Reims', 'Troyes', 'Paris', 'Nancy']
    assert dmin == 810  and  (vmin_nancy == attendu or vmin_nancy[::-1] == attendu)
    print("tests réussis pour meilleur_circuit_glouton")

    
#test_meilleur_circuit_glouton()