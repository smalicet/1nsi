import random

def fusion_place(tab, g, m, d, aux):
    """
    Fusionne les sous-tableaux tab[g:m] et tab[m:d]  
    triés dans l'ordre croissant en un sous-tableau tab[g:d]
    dans l'ordre croissant. 
    
    Parameters :
    tab : tableau d'entiers
    g, m, d : trois entiers
    aux : tableau d'entiers pour stocker provisoirement
         le tableau fusionné dans l'ordre croissant
    Préconditions : 
        tab[g:m] et tab[m:d]  dans l'ordre croissant
        0 <= g < m < d <= len(tab)
        len(tab) <= len(aux)

    Returns:  None
    """
    assert 0 <= g <= m <= d <= len(tab)
    assert len(tab) <= len(aux)
    i1 = g # indice dans tab[g:m]
    i2 = m  # indice dans tab[m:d]
    i3 = 0 # indice dans aux
    while i1 < m and ...:
        if tab[i1] <= tab[i2]:
            aux[i3] = tab[i1]
            i1 = i1 + 1
        else:
            # à compléter
            ...
        i3 = i3 + 1
    # à compléter
    ...
    # on recopie aux dans tab[debut:fin]
    for k in range(g, d):
        tab[k] = aux[k - g]

        
def test_fusion_place():
    aux = [0 for _ in range(8)]
    # test 1 : tableau fusionné = tableau complet
    t1 = [1, 10, 20, 5, 8, 9, 15, 25]
    fusion_place(t1, 0, 3, 8, aux)
    assert t1 == [1, 5, 8, 9, 10, 15, 20, 25]
    # test 2 : tableau fusionné = sous-tableau
    t2 = [50, 10, 20, 5, 22, 30, 7, 4]
    fusion_place(t2, 1, 3, 6, aux)
    assert t2 == [50, 5, 10, 20, 22, 30, 7, 4]
    # test 3 : fusion de deux sous-tableaux avec un élément
    t3 = [50, 40, 20, 15, 7]
    fusion_place(t3, 1, 2, 3, aux)
    assert t3 == [50, 20, 40, 15, 7]
    # test 4 : fusion avec deux sous-tableaux touchant un bord
    t4 = [50, 12, 18, 10, 15, 17]
    fusion_place(t4, 1, 3, 6, aux)
    assert t4 == [50, 10, 12, 15, 17, 18]
    print("tests réussis")
    
    
def tri_fusion_place(tab, g, d, aux):
    """
    Tri fusion en place et récursif de tab[g;d]
    avec aux comme tableau auxilaire de stockage pour la fusion

    Parameters
    ----------
    tab : tableau d'entiers
    g, d : deux entiers
    aux : tableau d'entiers
    
    Préconditions :
        0 <= g < d <= len(tab)
        len(aux) >= len(tab)
    Returns
    -------
    None.
    """
    # Préconditions
    assert 0 <= g <= d <= len(tab)
    assert len(aux) >= len(tab)
    n = d - g
    if n <= 1:  # cas de base : tableau à 1 élément déjà trié
        return
    else:  # appels récursifs
        # Diviser
        m = (g + d) // 2       
        # Résoudre les deux sous-problèmes
         # à compléter
        ...
        # Combiner les solutions
        # à compléter
        ...
        
def tri_fusion_place_enveloppe(tab):
    """
    Fonction enveloppe pour tri fusion en place du tableau d'entiers tab
    """
    tri_fusion_place(..., ..., ..., ...)

def test_tri_fusion_place():
    for taille in range(10):
        for _ in range(20):
            tab = [random.randint(0, taille) for _ in range(taille)]
            aux = [0 for _ in range(taille)]
            tri_fusion_place(tab, 0, taille, aux)
            assert tab == sorted(tab)
    print("tests réussis")