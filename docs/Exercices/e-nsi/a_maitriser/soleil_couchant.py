def nb_batiments_eclaires(hauteurs):
    n = 0
    maxi = 0
    for i in range(len(hauteurs)):
        if hauteurs[i] > maxi:
            maxi = hauteurs[i]
            n = n + 1
    return n




# tests

assert 4 == nb_batiments_eclaires([2, 1, 2, 4, 0, 4, 5, 3, 5, 6])
assert 1 == nb_batiments_eclaires([0, 3, 1, 2])

