#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 11:33:33 2023

@author: fjunier
"""
def occurrence_caracteres(phrase):
    occ = dict()
    for c in phrase:
        if c not in occ:
            occ[c] = 1
        else:
            occ[c] = occ[c] + 1
    return occ




# tests

assert occurrence_caracteres("Bonjour à tous !") == {
    'B': 1, 'o': 3, 'n': 1, 'j': 1, 'u': 2, 'r': 1,
    ' ': 3, 'à': 1, 't': 1, 's': 1, '!': 1
}

assert occurrence_caracteres("ababbab") == {"a": 3, "b": 4}

