#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 14:45:59 2023

@author: fjunier
"""

def nb_zeros(n):
    z = 0
    while n % 10 == 0:
        z = z + 1
        n = n // 10
    return z
        




# tests

assert nb_zeros(42000) == 3
assert nb_zeros(3210) == 1
assert nb_zeros(282475249) == 0
assert nb_zeros(7**10000) == 0
assert nb_zeros(7**10000 * 1000) == 3