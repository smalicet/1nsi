#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 15:02:48 2022

@author: fjunier
"""
def inversions(tableau):
    return sum(sum(1 if tableau[i] > tableau[j] else 0 for j in range(i +1, len(tableau))) for i in range(len(tableau)))


# Tests
assert inversions([]) == 0
assert inversions([5, 6, 7, 9]) == 0
assert inversions([7, 5, 9, 6]) == 3
