#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 21:15:43 2022

@author: fjunier
"""
def tous_differents(tableau):
    for i in range(len(tableau)):
        for j in range(i + 1, len(tableau)):
            if tableau[i] == tableau[j]:
                return False
    return True


# tests

tableau1 = [1, 2, 3, 6, 2, 4, 5]
assert tous_differents(tableau1) == False

tableau2 = ['chien', 'chat', 'lion', 'poisson']
assert tous_differents(tableau2) == True
