#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 21:42:23 2023

@author: fjunier
"""
def rendu(somme_a_rendre):
    pieces = [5, 2, 1]
    rendu = [0 for _ in range(len(pieces))]
    for k in range(len(pieces)):
        p = pieces[k]
        while somme_a_rendre >= p:
            somme_a_rendre = somme_a_rendre  - p 
            rendu[k] = rendu[k] + 1
    return tuple(rendu)
        




# tests

assert tuple(rendu( 7)) == (1, 1, 0)
assert tuple(rendu(10)) == (2, 0, 0)
assert tuple(rendu(13)) == (2, 1, 1)
assert tuple(rendu(32)) == (6, 1, 0)
