SYMBOLES = "0123456789ABCDEF"
HEX_DEC = {SYMBOLES[k]: k for k in range(len(SYMBOLES))}

def hex_int(seizaine, unite):
    """à compléter"""
    return 16 * HEX_DEC[seizaine] + HEX_DEC[unite]

def html_vers_rvb(html):
    """à compléter"""
    return tuple(hex_int(html[1 + k], html[2 + k]) for k in [0, 2, 4])


# test

assert hex_int('B', '5') == 181, "Échec hex_int exemple 1 de l'énoncé"
assert hex_int('0', '0') == 0, "Échec hex_int exemple 2 de l'énoncé"

assert html_vers_rvb("#C0392B") == (192, 57, 43), "Échec html_vers_rvb exemple 1"
assert html_vers_rvb("#00FF00") == (0, 255, 0), "Échec html_vers_rvb exemple 2"
assert html_vers_rvb("#000000") == (0, 0, 0), 'Échec html_vers_rvb exemple 3'
