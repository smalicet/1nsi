#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 21:15:43 2022

@author: fjunier
"""
def aplatir(matrice):
    return [matrice[i][j] for i in range(len(matrice)) for j in range(len(matrice[i]))]


# Tests
assert aplatir([[1, 2, 3, 4], [5, 6, 7, 8]]) == [1, 2, 3, 4, 5, 6, 7, 8]
assert aplatir([[1, 2, 3], [4, 5, 6]]) == [1, 2, 3, 4, 5, 6]
assert aplatir([[1, 2], [3, 4], [5, 6]]) == [1, 2, 3, 4, 5, 6]
assert aplatir([[1], [2], [3], [4], [5], [6]]) == [1, 2, 3, 4, 5, 6]

