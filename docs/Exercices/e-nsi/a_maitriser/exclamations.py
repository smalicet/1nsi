def nb_max_consecutifs1(motif, phrase):
    n = 0
    nmax = 0
    for c in phrase:
        if c != motif:
            if n > nmax:
                nmax = n
            n = 0
        else:
            n += 1
    if n > nmax:
        nmax = n
    return nmax
            
def nb_max_consecutifs(motif, phrase):
    nmax = 0
    n = 0
    for c in phrase:
        if c == motif:
            n += 1
            if n > nmax:
                nmax = n
        else:
            n = 0
    return nmax



# tests

phrase = "Dans une phrase !!! écrite !!! certains utilisateurs abusent des points d'exclamations !! Ce pour différentes raisons ! Bref."
assert nb_max_consecutifs("!", phrase) == 3

phrase = "Un mot    puis        un        autre avec espaces."
assert nb_max_consecutifs(" ", phrase) == 8

expression = "((2 * x + 3) / (x + 1))"
assert nb_max_consecutifs("(", expression) == 2
assert nb_max_consecutifs("-", expression) == 0
