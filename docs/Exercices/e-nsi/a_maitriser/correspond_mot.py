#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 21:51:09 2023

@author: fjunier
"""
def correspond(mot_complet, mot_a_trous):
    if len(mot_complet) != len(mot_a_trous):
        return False
    for k in range(min(len(mot_complet), len(mot_a_trous))):
        if mot_a_trous[k] !=  '.' and  mot_a_trous[k] != mot_complet[k]:
            return False
    return True





# tests

assert correspond("INFORMATIQUE", "INFO.MA.IQUE") == True
assert correspond("AUTOMATIQUE", "INFO.MA.IQUE") == False
assert correspond("INFO", "INFO.MA.IQUE") == False
assert correspond("INFORMATIQUES", "INFO.MA.IQUE") == False
