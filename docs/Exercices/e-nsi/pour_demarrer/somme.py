#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 15:15:42 2023

@author: fjunier
"""
def somme(a, b):
    return a + b# ← compléter ici


# tests

assert somme(10, 32) == 42
assert somme(100, 7) == 107
