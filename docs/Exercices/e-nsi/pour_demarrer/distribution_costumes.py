#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 15:18:41 2023

@author: fjunier
"""
def costume(rang, couleurs):
   return couleurs[(rang - 1) % len(couleurs)]

# Tests
assert costume(2, ["bleu", "blanc", "rouge"]) == "blanc"
assert costume(8, ["rose", "vert", "orange", "bleu"]) == "bleu"