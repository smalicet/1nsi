#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 21:23:41 2024

@author: fjunier
"""


def recherche(cible, grille):
    n = nb_lignes(grille)
    m = nb_colonnes(grille)
    i = 0
    j = m - 1
    while (i < n) and (j >= 0):
        x = donne_valeur(grille, i, j)
        if x > cible:
            j -= 1
        elif x < cible:
            i += 1
        else:
            return (i, j)
    return None


def recherche2(cible, grille):
    n = nb_lignes(grille)
    m = nb_colonnes(grille)
    i = 0
    j = m - 1
    k = 0
    v = donne_valeur(grille, i, j)
    while True:
        i0, j0 = i, j
        if k % 2 == 0:
            if 0 <= j and v > cible:
                j = j - 1
            if j < 0:
                return None
        else:
            if i < n and v < cible:
                i += 1
            if i == n:
                return None
        k += 1
        if (i, j) != (i0, j0):
            v = donne_valeur(grille, i, j)
        if v == cible:
            return (i, j)


def recherche3(cible, grille):
    n = nb_lignes(grille)
    m = nb_colonnes(grille)
    i = 0
    j = m - 1
    while True:
        i0, j0 = i, j
        while 0 <= j and donne_valeur(grille, i, j) > cible:
            j = j - 1
        if j < 0:
            return None
        while i < n and donne_valeur(grille, i, j) < cible:
            i += 1
        if i == n:
            return None
        if (i, j) == (i0, j0):
            return (i, j)


#%%
# tests

# Pour ce test public grille est une liste Python,
#  mais ce ne sera pas toujours le cas !

grille = [
    [11, 33, 42, 63],
    [20, 52, 67, 80],
    [25, 61, 88, 95],
]


def nb_lignes(grille):
    return len(grille)


def nb_colonnes(grille):
    return len(grille[0])


def donne_valeur(grille, i, j):
    global cout
    assert 0 <= i < 3
    assert 0 <= j < 4
    cout += 1
    return grille[i][j]


cout = 0
resultat = recherche(42, grille)
print(cout)
assert cout <= 7, "Trop de tentatives"
assert resultat == (0, 2), "Mauvaises coordonnées"


cout = 0
resultat = recherche(24, grille)
print(cout)
assert cout <= 7, "Trop de tentatives"
assert resultat is None, "La cible est absente"

#%%
from random import randrange

grille = None


def recherche(cible, grille):
    n = nb_lignes(grille)
    m = nb_colonnes(grille)
    i = 0
    j = m - 1
    while True:
        i0, j0 = i, j
        while 0 <= j and donne_valeur(grille, i, j) > cible:
            j = j - 1
        if j < 0:
            return None
        while i < n and donne_valeur(grille, i, j) < cible:
            i += 1
        if i == n:
            return None
        if (i, j) == (i0, j0):
            return (i, j)


def nb_lignes(grille):
    return 1000


def nb_colonnes(grille):
    return 1000


def donne_valeur(grille, i, j):
    global cout_secret
    cout_secret += 1
    return (1000 * 1000 + i * 1000 + j) * 2


from random import randrange

cout_secret = 0
resultat = recherche(randrange(1, 10**6), grille)
trop = cout_secret > 2000
assert trop == False, "Trop de tentatives"
assert resultat is None, "La cible est absente"

for _ in range(10):
    i0 = randrange(0, 1000)
    j0 = randrange(0, 1000)
    cible = (1000 * 1000 + i0 * 1000 + j0) * 2

    cout_secret = 0
    resultat = recherche(cible, grille)
    trop = cout_secret > 2000
    assert trop == False, "Trop de tentatives"
    assert resultat == (i0, j0), "Mauvaises coordonnées"

    cout_secret = 0
    resultat = recherche(cible + 1, grille)
    print(cout_secret)
    trop = cout_secret > 2000
    assert trop == False, "Trop de tentatives"
    assert resultat is None, "cible absente"
