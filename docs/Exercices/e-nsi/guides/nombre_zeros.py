#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 23:42:30 2023

@author: fjunier
"""


def nb_zeros(n):
    c = 0
    while n % 10 == 0:
        c = c + 1
        n = n // 10
    return c


# tests

assert nb_zeros(42000) == 3
assert nb_zeros(3210) == 1
assert nb_zeros(282475249) == 0
assert nb_zeros(7**10000) == 0
assert nb_zeros(7**10000 * 1000) == 3
