import math
import doctest

class Point:
    """Classe de fabrication d'un point du plan"""
    
    def __init__(self, x, y):
        """Constructeur d'un point à partir de ses coordonnées
        >>> p1 = Point(0, 0)
        """
        self.x = x
        self.y = y
        
    def distance(self, autre):
        """Méthode qui renvoie la distance d'un point à un autre point"""
        return math.sqrt((self.x - autre.x) ** 2 + (self.y - autre.y) ** 2)
    


# On exécute les tests inclus dans les docstrings
#à décommenter
#doctest.testmod(verbose=True)
