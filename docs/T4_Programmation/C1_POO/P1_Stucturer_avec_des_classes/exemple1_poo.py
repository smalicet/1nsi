class Vecteur:
    """Classe de fabrication d'un vecteur du plan"""
    
    def __init__(self, x, y):
        """Constructeur de la classe Vecteur"""
        self.x  = x  # attribut x
        self.y = y   # attribut y 
    
    def addition(self, autre):
        """Méthode d'addition à un autre vecteur"""
        return Vecteur(self.x + autre.x, self.y + autre.y)
    
class Temps:
    """Classe de fabrication d'un temps"""
    
    def __init__(self, h, m, s):
        """Constructeur de la classe Temps"""
        self.h = h
        self.m = m
        self.s = s
    
    def addition(self, autre):
        """Méthode d'addition à un autre temps"""
        h1, m1, s1 = self.h, self.m, self.s
        h2, m2, s2 = autre.h, autre.m, autre.s
        s3 = (s1 + s2) % 60
        m3 = (m1 + m2 + (s1 + s2) // 60) % 60
        h3 = h1 + h2 + (m1 + m2 + (s1 + s2) // 60) // 60
        return Temps(h3, m3, s3)

w1 = Vecteur(10, -4)  # construction d'un premier vecteur
w2 = Vecteur(-3, 6)  # construction d'un second vecteur
w3 = w1.addition(w2) # appel de méthode d'addition de w1 à w2
print("Affichage de w3 : ", w3)
print("Composante x de w3 :", w3.x, "Composante y de w3 : ", w3.y)

t1 = Temps(1, 59, 45) # construction d'un premier temps
t2 = Temps(2, 0, 20)  # construction d'un second temps
t3 = t1.addition(t2)  # appel de méthode d'addition de t1 à t2
print("Affichage de t3 : ", t3)
print("Composante h de t3 :", t3.h, "Composante m de t3 : ", t3.m, "Composante s de t3 : ", t3.s)
