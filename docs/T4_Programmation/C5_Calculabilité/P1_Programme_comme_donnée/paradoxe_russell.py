from detecteur_parfait import detecteur_parfait

barbier = ['Un barbier rase tous les hommes', 
           'qui ne se rasent pas eux-mêmes', 
           'et seulement ceux-là.', 
           'Le barbier se rase-t-il lui-même ?']


f = open('paradoxe_russell.py')
prog = f.read()
f.close()

if detecteur_parfait(prog):
    print("IndexError detected")
else:
    print("No IndexError detected")
    print(barbier[len(barbier)])
