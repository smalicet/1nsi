humble_programmer = ['Program testing',
 'can be a very effective way ',
 'to show the presence of bugs,',
 'but is hopelessly inadequate',
 'for showing their absence.',
 'Edsger Dijkstra']

i = 5
while True:
    print(humble_programmer[i])
    i = (i + 1) % 6
print(humble_programmer[6])
