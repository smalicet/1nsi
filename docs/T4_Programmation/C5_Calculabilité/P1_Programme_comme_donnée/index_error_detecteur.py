prog1 = '''
humble_programmer = ['Program testing',
 'can be a very effective way ',
 'to show the presence of bugs,',
 'but is hopelessly inadequate',
 'for showing their absence.',
 'Edsger Dijkstra']


for i in range(6):
    print(humble_programmer[i])
'''

prog2 = '''
humble_programmer = ['Program testing',
 'can be a very effective way ',
 'to show the presence of bugs,',
 'but is hopelessly inadequate',
 'for showing their absence.',
 'Edsger Dijkstra']


for i in range(7):
    print(humble_programmer[i])
'''


prog3 = '''
humble_programmer = ['Program testing',
 'can be a very effective way ',
 'to show the presence of bugs,',
 'but is hopelessly inadequate',
 'for showing their absence.',
 'Edsger Dijkstra']

i = 5
while True:
    print(humble_programmer[i])
    i = (i + 1) % 6
print(humble_programmer[6])
'''


def index_error_detecteur(donnée):
    try:
        exec(donnée)  # exécution de donnée comme programme
        return False
    except IndexError:
        return True
    except:
        return False
    
