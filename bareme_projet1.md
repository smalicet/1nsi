Compétence évaluée|note obtenue|barème|outils d'évaluation
--:|:--:|:--:|:--
Investissement dans le projet|                  |5 |Journal de bord<br/>Attitude en classe<br/>Quantité de travail fourni
Contenu du site web |               |2| Le contenu est personnalisé<br/>Le contenu est cohérent et intéressant
Nombre de pages achevées |             |3|Le site comporte au minimum 4 pages par élève<br/>Les pages sont riches en contenus (liens, images, etc...)
Insertion de liens |                 |1|Présence de liens internes au site<br/>Présence de liens vers d'autres sites vérifiés  
Présence des sources |                    |1| Les sources et licences des images ou autres documents utilisés sont présentes
Insertion de code Javascript |             |3|Certaines pages du site possèdent du code javascript<br/>Qualité du javascript<br/>Variété dans   l'utilisation du javascript
Le fichier style |                 |2|Le style est cohérent sur toutes les pages<br/>Le style est complexe avec de nombreux paramètres
Rendu final du projet |                      |3|Appréciation personnelle sur le rendu final 
**Total**|                                  |**20**|